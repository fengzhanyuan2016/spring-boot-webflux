package com.bmw.webflux.controller;

import com.bmw.webflux.domain.User;
import com.bmw.webflux.repository.IUserRepository;
import com.bmw.webflux.util.CheckUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;


@RestController
@RequestMapping("/user")
public class UserController {
    private final IUserRepository userRepository;
    public UserController(IUserRepository userRepository){
        this.userRepository=userRepository;
    }


    @GetMapping("/")
    public Flux<User>getAll(){
        return userRepository.findAll();
    }
    @GetMapping(value = "/stream/all",produces =MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User>getStreamAll(){
        return userRepository.findAll();
    }
    @PostMapping("/")
    public Mono<User>createUser(@Valid @RequestBody User user){

        //新增和修改都是save 修改传id
        CheckUtil.checkName(user.getName());
        return this.userRepository.save(user);
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>>deleteUser(@PathVariable("id") String id){

        //deletebyid不能判断数据是否存在
        //this.userRepository.deleteById(id);
       return this.userRepository.findById(id)
                .flatMap(user->this.userRepository.deleteById(user.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));

    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<User>> updateUser(@PathVariable("id") String id,@Valid @RequestBody User user){
       CheckUtil.checkName(user.getName());
       return this.userRepository.findById(id).flatMap(u->{
            u.setAge(user.getAge());
            u.setName(user.getName());
            return userRepository.save(u);
        }).map(u->new ResponseEntity<User>(u,HttpStatus.OK)).defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<User>> findById(@PathVariable("id") String id){
        return this.userRepository.findById(id)
                .map(u->new ResponseEntity<User>(u,HttpStatus.OK)).defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/age/{start}/{end}")
    public Flux<User>findByAge(@PathVariable("start")int start,@PathVariable("end")int end){
        return userRepository.findByAgeBetween(start,end);
    }

    @GetMapping(value = "/stream/age/{start}/{end}",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User>findStreamByAge(@PathVariable("start")int start,@PathVariable("end")int end){
        return userRepository.findByAgeBetween(start,end);
    }


    @GetMapping("/old")
    public Flux<User>getOld(){
        return userRepository.oldUser();
    }

    @GetMapping(value = "/stream/old",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User>getStreamOld(){
        return userRepository.oldUser();
    }


}
