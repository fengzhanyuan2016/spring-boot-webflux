package com.bmw.webflux.repository;

import com.bmw.webflux.domain.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface IUserRepository extends ReactiveMongoRepository<User,String> {
    Flux<User> findByAgeBetween(int start,int end);

    @Query("{'age':{'$gte':30,'$lte':50}}")
    Flux<User> oldUser();
}
