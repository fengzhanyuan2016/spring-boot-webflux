package com.bmw.webflux.util;

import com.bmw.webflux.exception.CheckException;

import java.util.stream.Stream;

public class CheckUtil {
    private static final String INVALID_NAME[]={"admin","guanliyuan"};
    //校验名字
    public static void checkName(String value){
        Stream.of(INVALID_NAME).filter(name->name.equalsIgnoreCase(value)).findAny().ifPresent(name->{
            throw new CheckException("name",value);
        });
    }
}
